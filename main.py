import asyncio
import logging
import websockets
import psycopg2 as pg2
import os

logging.basicConfig(level=logging.INFO)

class WebSocketClient:

    def __init__(self):
        self.is_alive = True
        try:
            self.conn = pg2.connect(dbname  = self.get_env_variable("POSTGRES_DB"),
                                    user    = self.get_env_variable("POSTGRES_USER"),
                                    password= self.get_env_variable("POSTGRES_PW"),
                                    host    = self.get_env_variable("POSTGRES_URL"))
        except:
            logging.info(f"I am unable to connect to the database.")
            exit()

    def get_env_variable(self, name):
        try:
            return os.environ[name]
        except KeyError:
            message = "Expected environment variable '{}' not set.".format(name)
            raise Exception(message)

    def close_connection_database(self):
        self.conn.close()

    async def alive(self):
        while self.is_alive:
            logging.info('alive')
            await asyncio.sleep(300)

    async def async_processing(self, hostname: str, token: str):
        websocket_resource_url = f"ws://{hostname}?{token}"

        async with websockets.connect(websocket_resource_url) as websocket:
            while True:
                try:
                    message = await websocket.recv()
                    data = message.replace("'", "\"")
                    with self.conn.cursor() as cursor:
                        try:
                            cursor.execute(f"INSERT INTO webhooks (data) VALUES(\'{data}\');")
                            self.conn.commit()
                            logging.info(f"{data} save to database!")
                        except pg2.Error as e:
                            cursor.close()
                            logging.info(f"{e.pgerror}")
                except websockets.exceptions.ConnectionClosed:
                    logging.info(f"Connection with web socket server closed")
                    self.is_alive = False
                    cursor.close()
                    break
                finally:
                    cursor.close()
                    logging.info(f"Next!")

if __name__ == '__main__':

    client = WebSocketClient()

    asyncio.get_event_loop().run_until_complete(asyncio.wait([
        client.alive(),
        client.async_processing(hostname=client.get_env_variable("WEB_SOCKET_HOST_NAME"),
                                token   =client.get_env_variable("WEB_SOCKET_TOKEN"))
    ]))

    client.close_connection_database()
