**DB:**

- PostgreSQL 12.5

**Create table:**

- CREATE TABLE webhooks (id serial PRIMARY KEY, data jsonb);

**Set Environment Variables:**

- export POSTGRES_URL="host"
- export POSTGRES_USER="nameUser"
- export POSTGRES_PW="password"
- export POSTGRES_DB="nameDB"
- export WEB_SOCKET_HOST_NAME="hostNameWebSocket"
- export WEB_SOCKET_TOKEN="token"

WebSocket connection. Saving data into DB.
